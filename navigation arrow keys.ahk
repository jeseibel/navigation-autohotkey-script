﻿; start of auto execute section

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.



;==============================
;  have number lock always on
;==============================

SetNumLockState, AlwaysOn


; end of auto execute section


;=======================================
; remap Windows+left/right to home/end
;=======================================

; For some reason Win+Left/Right doesn't override
; the default functionality unless I manually send Home/End
#Left:: 
Send {Home}
Return

; same as above but also captures
; and sends shift presses
#+Left::
; LWin down is to prevent the windows key from being incorrectly fired when the windows key is released
Send {LWin down} {LShift down}{Home} 
Return


#Right:: 
Send {End}
Return

#+Right::
Send {LWin down} {LShift down}{End}
Return


;=======================================
; remap Windows+up/down to page up/down
;=======================================

#Up:: 
Send {PgUp}
Return

#+Up:: 
Send {LWin down} {LShift down}{PgUp}
Return

#^Up::
Send {LWin down} {LCtrl down}{PgUp}
Return


#Down:: 
Send {PgDn}
Return

#+Down:: 
Send {LWin down} {LShift down}{PgDn}
Return

#^Down::
Send {LWin down} {LCtrl down}{PgDn}
Return



A script to remap windows key + Up/Down/Left/Right to PageUp/PageDown/Home/End respectively. To improve typing speed and comfort when moving from a Dell laptop to a keyboard that doesn't have those built in.

To have the script auto start make sure to add a shortcut to it in the windows startup folder, located here:
C:\Users\YOUR_USER_NAME\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup